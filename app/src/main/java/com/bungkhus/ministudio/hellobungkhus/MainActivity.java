package com.bungkhus.ministudio.hellobungkhus;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.support.annotation.NonNull;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.firebase.ui.storage.images.FirebaseImageLoader;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.database.ChildEventListener;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import droidninja.filepicker.FilePickerBuilder;
import droidninja.filepicker.FilePickerConst;

import static android.R.attr.id;

public class MainActivity extends AppCompatActivity {

    private FirebaseAuth mFirebaseAuth;
    private FirebaseUser mFirebaseUser;
    private DatabaseReference mDatabase;
    private String mUserId;
    private ArrayList<String> filepaths = new ArrayList<>();
    FirebaseStorage storage = FirebaseStorage.getInstance();
    StorageReference storageRef = storage.getReferenceFromUrl("gs://bungkhus-e28d3.appspot.com");
    StorageReference imageRef = storageRef.child("images");
    StorageReference docRef = storageRef.child("doc");
    private ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set up actionBar
        ActionBar actionBar = getSupportActionBar();
        actionBar.setTitle("Loading...");

        // Initialize Firebase Auth and Database Reference
        mFirebaseAuth = FirebaseAuth.getInstance();
        mFirebaseUser = mFirebaseAuth.getCurrentUser();
        mDatabase = FirebaseDatabase.getInstance().getReference();

        if (mFirebaseUser == null) {
            // Not logged in, launch the Log In activity
            loadLogInView();
        } else {
            mUserId = mFirebaseUser.getUid();

            // Set up ListView
            final ListView listView = (ListView) findViewById(R.id.listView);
            final ArrayAdapter<String> adapter = new ArrayAdapter<>(this, android.R.layout.simple_list_item_1, android.R.id.text1);
            listView.setAdapter(adapter);

            // Add items via the Button and EditText at the bottom of the view.
            final EditText text = (EditText) findViewById(R.id.todoText);
            final Button button = (Button) findViewById(R.id.addButton);
            button.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Item item = new Item(text.getText().toString());
                    mDatabase.child("users").child(mUserId).child("items").push().setValue(item);
                    text.setText("");
                }
            });

            mDatabase.child("users").child(mUserId).addValueEventListener(new ValueEventListener() {
                @Override
                public void onDataChange(DataSnapshot dataSnapshot) {
                    //Getting the data from snapshot
                    User user = dataSnapshot.getValue(User.class);
                    ActionBar actionBar = getSupportActionBar();
                    actionBar.setTitle("hi, " + user.getName());
                    actionBar.setSubtitle(user.getEmail());
                }

                @Override
                public void onCancelled(DatabaseError databaseError) {
                    System.out.println("value onCancelled");
                }
            });

            // Use Firebase to populate the list.
            mDatabase.child("users").child(mUserId).child("items").addChildEventListener(new ChildEventListener() {
                @Override
                public void onChildAdded(DataSnapshot dataSnapshot, String s) {
                    adapter.add((String) dataSnapshot.child("title").getValue());
                }

                @Override
                public void onChildChanged(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onChildRemoved(DataSnapshot dataSnapshot) {
                    adapter.remove((String) dataSnapshot.child("title").getValue());
                }

                @Override
                public void onChildMoved(DataSnapshot dataSnapshot, String s) {

                }

                @Override
                public void onCancelled(DatabaseError databaseError) {

                }
            });

            // Delete items when clicked
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                public void onItemClick(AdapterView< ?> parent, View view, int position, long id) {
                    confirmDialog(MainActivity.this, listView, position);
                }
            });
        }

        loadImage("bicycle-1280x720.jpg");
    }

    private void confirmDialog(Context context, final ListView listView, final int position) {
        AlertDialog.Builder builder = new AlertDialog.Builder(context);

        builder
                .setMessage("Are you sure want to delete this post?")
                .setPositiveButton("Yes",  new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // Yes-code
                        mDatabase.child("users").child(mUserId).child("items")
                                .orderByChild("title")
                                .equalTo((String) listView.getItemAtPosition(position))
                                .addListenerForSingleValueEvent(new ValueEventListener() {
                                    @Override
                                    public void onDataChange(DataSnapshot dataSnapshot) {
                                        if (dataSnapshot.hasChildren()) {
                                            DataSnapshot firstChild = dataSnapshot.getChildren().iterator().next();
                                            firstChild.getRef().removeValue();
                                        }
                                    }

                                    @Override
                                    public void onCancelled(DatabaseError databaseError) {

                                    }
                                });
                    }
                })
                .setNegativeButton("No", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog,int id) {
                        dialog.cancel();
                    }
                })
                .show();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle item selection
        switch (item.getItemId()) {
            case R.id.action_logout:
                mFirebaseAuth.signOut();
                loadLogInView();
                return true;
            case R.id.action_choose_file:
                FilePickerBuilder.getInstance().setMaxCount(1)
                        .setSelectedFiles(filepaths)
                        .setActivityTheme(R.style.AppTheme)
                        .pickDocument(this);
                return true;
            case R.id.action_choose_image:
                FilePickerBuilder.getInstance().setMaxCount(1)
                        .setSelectedFiles(filepaths)
                        .setActivityTheme(R.style.AppTheme)
                        .pickPhoto(this);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode)
        {
            case FilePickerConst.REQUEST_CODE_DOC:
                if(resultCode==RESULT_OK && data!=null)
                {
                    filepaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_DOCS);
                    //use them anywhere
                    if (filepaths.size() > 0) {
                        showProgressDialog();

                        System.out.println("@#docs = " + filepaths);
                        Uri file = Uri.fromFile(new File(filepaths.get(0)));
                        StorageReference nameRef = storageRef.child("doc/"+file.getLastPathSegment());
                        uploadFile(nameRef, file, "doc", file.getLastPathSegment());
                    } else {
                        System.out.println("no file selected");
                    }
                }
                break;
            case FilePickerConst.REQUEST_CODE_PHOTO:
                if(resultCode==RESULT_OK && data!=null)
                {
                    filepaths = data.getStringArrayListExtra(FilePickerConst.KEY_SELECTED_PHOTOS);
                    //use them anywhere
                    if (filepaths.size() > 0) {
                        showProgressDialog();

                        System.out.println("@#image = " + filepaths);
                        Uri file = Uri.fromFile(new File(filepaths.get(0)));
                        StorageReference nameRef = storageRef.child("images/"+file.getLastPathSegment());
                        uploadFile(nameRef, file, "image", file.getLastPathSegment());
                    } else {
                        System.out.println("no photo selected");
                    }
                }
                break;
        }
    }

    private void uploadFile(StorageReference nameRef, Uri file, final String state, final String fileName) {
        UploadTask uploadTask = nameRef.putFile(file);
        uploadTask.addOnFailureListener(new OnFailureListener() {
            @Override
            public void onFailure(@NonNull Exception exception) {
                // Handle unsuccessful uploads
                Log.e("Upload Failure", exception.getMessage());
                Toast.makeText(MainActivity.this, "Upload Failure", Toast.LENGTH_LONG).show();
                hideProgressDialog();
            }
        }).addOnSuccessListener(new OnSuccessListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onSuccess(UploadTask.TaskSnapshot taskSnapshot) {
                // taskSnapshot.getMetadata() contains file metadata such as size, content-type, and download URL.
                Uri downloadUrl = taskSnapshot.getDownloadUrl();
                Log.e("Upload Success", downloadUrl.toString());
                Toast.makeText(MainActivity.this, "Upload Success", Toast.LENGTH_LONG).show();
                hideProgressDialog();
                if (state.equalsIgnoreCase("image")) {
                    loadImage(fileName);
                }
            }
        });
    }

    private void loadImage(String image) {
        StorageReference storageReference = imageRef.child(image);

        // ImageView in your Activity
        ImageView imageView = (ImageView) findViewById(R.id.imageView);

        // Load the image using Glide
        Glide.with(this /* context */)
                .using(new FirebaseImageLoader())
                .load(storageReference)
                .into(imageView);
    }

    private void loadLogInView() {
        Intent intent = new Intent(this, LogInActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK);
        startActivity(intent);
    }

    public void showProgressDialog() {
        if (mProgressDialog == null) {
            mProgressDialog = new ProgressDialog(this);
            mProgressDialog.setMessage(getString(R.string.loading));
            mProgressDialog.setIndeterminate(true);
        }

        mProgressDialog.show();
    }

    public void hideProgressDialog() {
        if (mProgressDialog != null && mProgressDialog.isShowing()) {
            mProgressDialog.dismiss();
        }
    }
}
