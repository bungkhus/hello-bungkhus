package com.bungkhus.ministudio.hellobungkhus;

/**
 * Created by bungkhus on 12/22/16.
 */

public class Item {

    private String title;

    public Item() {}

    public Item(String title) {
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
